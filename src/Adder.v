`default_nettype none
module Adder #(parameter n = 1, m = 0) (
    input signed [n-1:0] operand1, operand2,
    output signed [n-1:0] result
);

    wire [n:0] out = operand1 + operand2;
    wire of = !(operand1[n-1]^operand2[n-1]) ? out[n] ^ operand1[n-1] : 1'b0;
    assign result = of ? {operand1[n-1],{n-1{!operand1[n-1]}}}:out[n-1:0];

endmodule