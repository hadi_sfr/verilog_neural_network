`default_nettype none
module Counter #(parameter n = 1) (
    input clk,    // Clock
    input clk_en, // Clock Enable
    input rst,    // reset
    output [n-1:0] value
);
    
    wire [n-1:0] next_value;
    Reg #(n) count(clk, clk_en, rst, next_value, value);
    assign next_value = value + 1'b1;

endmodule