`default_nettype none
module ActivationFunction #(parameter n = 1, m = 0) (
    input signed [n-1:0] in,
    output signed [n-1:0] out
);

    assign out = in[n-1] ? 0 : in;

endmodule