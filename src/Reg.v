`default_nettype none
module Reg #(parameter n = 1) (
    input clk,
    input ld_en,
    input rst,
    input [n-1:0] parallel_in,
    output reg [n-1:0] out
);

    always@(posedge clk) begin
      if(rst)
        out <= 0;
      else if(ld_en)
        out <= parallel_in;
    end

endmodule