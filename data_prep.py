#! /usr/bin/env python3


def main():
    length = 62
    dataset = []
    with open("hex_data_matlab/te_data.hex") as f:
        lines = f.readlines()
        lines = [line[4:-1] for line in lines]
        for i in range(len(lines)//length):
            dataset.append(reversed(lines[i*length:(i+1)*length]))
    with open("hex_data/data.hex", "w") as f:
        for data in dataset:
            print("".join(data), file=f)


if __name__ == '__main__':
    main()
